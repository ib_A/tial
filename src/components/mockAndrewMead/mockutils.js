// import moment from "moment";
const moment = require.requireActual('moment');
const timer = (timeStamp = 0) => {
  return moment(timeStamp);
};
export default timer;
