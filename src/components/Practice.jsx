import React, { useState, useEffect } from "react";
function App() {
  let [counter, setCounter] = useState(0);
  return <button onClick={() => setCounter((x) => x + 1)}>{counter}</button>;
}

export default App;
