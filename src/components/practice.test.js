import React from "react";
import ReactDOM from "react-dom";
import { act } from "react-dom/test-utils";
import { render, screen } from "@testing-library/react";

import App from "./Practice";

it("should render 1", () => {
  // import React, { useState, useEffect } from "react";
  // function App() {
  //   let [ctr, setCtr] = useState(0);
  //   useEffect(() => {
  //     setCtr(1);
  //   }, []);
  //   return <button>{ctr}</button>;
  // }
  // export default App;
  //1) This does not work throw error that it is 0.
  //   Still uses useState value for assertion and not useEffect
  //   const container = document.createElement("div");
  //   document.body.appendChild(container);
  //   ReactDOM.render(<App />, container);
  //   const button = container.querySelector('button');
  //   expect(button.innerHTML).toBe("1"); // this fails!
  //2) This works because it is wrapped in act()
  //   const container = document.createElement("div");
  //   act(() => {
  //     document.body.appendChild(container);
  //     const button = container.querySelector("button");
  //     ReactDOM.render(<App />);
  //     expect(button.innerHTML).toBe("1"); // this fails!
  //   });
  //3) render of @testing-library/react is wrapped in act()
  //   this does not work throw error that it is 0. Still uses
  //   render(<App />);
  //   const button = screen.getByRole("button");
  //   expect(button.innerHTML).toBe("1");
});
it("should increment a counter", () => {
  const el = document.createElement("div");
  document.body.appendChild(el);
  // we attach the element to document.body to ensure events work
  ReactDOM.render(<App />, el);
  const button = el.childNodes[0];
  //   for (let i = 0; i < 3; i++) {
  //     button.dispatchEvent(new MouseEvent("click", { bubbles: true }));
  //   }
  act(() => {
    for (let i = 0; i < 3; i++) {
      button.dispatchEvent(new MouseEvent("click", { bubbles: true }));
    }
  });
  expect(button.innerHTML).toBe("3");
});
